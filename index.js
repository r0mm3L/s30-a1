
const express = require('express');
// const { Mongoose } = require('mongoose');
const mongoose = require('mongoose');




const app = express();


const port = 4000;


app.use(express.json())
	
app.use(express.urlencoded({extended: true}));


//mongodb+srv://admin:<password>@zuitt-bootcamp.cpkli.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

// syntax:
    // mongoose.connect('<connection string>, (middlewares)')

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.cpkli.mongodb.net/session30?retryWrites=true&w=majority', {

        useNewUrlParser: true,
        useUnifiedTopology: true
    });
let db = mongoose.connection;

    //console.error.bind(console) - print errors in the browser and in the terminal
    db.on('error', console.error.bind(console, "connection error"))
    // if the connection is successful, this will be the output in our console. 
    db.once('open', () => console.log('Connected to the cloud database'))


    // mongoose schema

    const taskSchema = new mongoose.Schema({

        // define the name of our schema - taskSchema
        // mongoose.Schema method
        // we will be needing the name of the task and its status
        // each field will require a data type. 

        name: String,
        status: {
            type: String,
            default: 'pending'
        }

    })


// models
const Task = mongoose.model('Task', taskSchema);
    // models use schema and they act as the middleman from the server to our database. 
    // model can now be used to run commands for interacting with our database.
    // naming convension name of model should be capitalised and singular form-'Task'
    // second parameter is used to specify the schema of the documents that will be stored in the mongodb collection. 

// business logic

/*

1. add a functionally to check if there are duplicate tasks
    -if  the task already exist in the db, we return an error
    -if the task does not exist in the db, we add it in the db.
2. the task data will be coing from the request body.
3. create a new task object with name field property. 

*/

app.post('/tasks', (req, res) =>{
    
    // check any duplicate task
    // err shorthand of error
    Task.findOne({name: req.body.name}, (err, result) => {

        // if there are  matches
        if(result != null && result.name === req.body.name) {

            // will return this message
            return res.send('Duplicate task found.')

        } else {
            
            let newTask = new Task({
                name: req.body.name
            })
            // save method will accept a callback function which stores any errors in the first parameter 
            // and will store the newly saved document in the second parameter.
            newTask.save((saveErr, savedTask) => {
                if(saveErr) {
                    return console.error(saveErr)
                } else {
                    return res.status(201).send('New Task Created')
                }
            })
        }
    })
})

// retrieving all tasks

app.get ('/tasks', (req, res) => {
    Task.find({}, (err, result) =>{
        if(err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data: result
            })
        }
    })
})


/*

ACTIVITY

*/

// schema

const registerSchema = new mongoose.Schema({

  
    username: String,
    password: String

})

// model

const Register = mongoose.model('Register', registerSchema);

// fucntionality

app.post('/signup', (req, res) =>{
    
    
    Register.findOne({name: req.body.username}, (err, result) => {

        
        if(result != null && result.username === req.body.username) {

            
            return res.send('Duplicate username found.')

        } else {
            
            let newRegister = new Register({
                username: req.body.username,
                password: req.body.password
            })
           
            newRegister.save((saveErr, savedRegister) => {
                if(saveErr) {
                    return console.error(saveErr)
                } else {
                    return res.status(201).send('New user Created')
                }
            })
        }
    })
})


app.get ('/users', (req, res) => {
    Register.find({}, (err, result) =>{
        if(err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data: result
            })
        }
    })
})


app.listen(port, () => console.log(`The Server is running at port ${port}`));